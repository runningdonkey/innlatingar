library(tidyverse)
library(openintro)
library(broom)
library(lubridate)
library(janitor)

## Ex. 1
# 1a
# Show output of bdims. How many observations?
bdims
# 507 observationer

# 1b
# Indeholder bdims nogen factors?
str(bdims)
# Niks. Indeholder kun nummerisk og intergers.

# 1c
# create tibble body as mutate of bdims...
# Change sex from integer til factor
is_tibble(bdims)
body <- bdims %>% 
  mutate(
    sex = factor(sex, levels=c(0,1)),
    sex = fct_recode(sex, f="0", m="1")
  )

# 1d
# select age, hgt and sex from body
body3 <- select(body,age,hgt,sex)
# View(body3)
body3
n_distinct(body3, na.rm = TRUE)
distinct(body3)

# 1e
# plot body
body %>% 
  ggplot(aes(x = sex, y = che_di)) +
  geom_boxplot()

body %>%
  ggplot(aes(x = che_di)) +
  geom_histogram(binwidth = 1) +
  facet_wrap(~sex, ncol = 1)

# 1f
# summarise count of observations and statistical descriptors
che_summary <- body %>%
  group_by(sex) %>%
  summarise(
    n = n(),
    mean = mean(che_di),
    sd = sd(che_di),
    min = min(che_di),
    q1 = quantile(che_di,0.25),
    median = median(che_di),
    q3 = quantile(che_di,0.75),
    max = max(che_di),
    IQR = IQR(che_di)
  )
che_summary
# XXX tekst om Independency og Normality-conditions fulfilled.
# compute and mutate to add t_df, SE, c_low and c_high
z_95 <- 1.96
che_summary %>%
  select(sex,n, mean, sd) %>%
  mutate(
    t_df = qt(0.975, n-1),
    SE = sd/sqrt(n),
    c_low = mean - z_95*SE, 
    c_high = mean + z_95*SE
  )
# also use a one-sample t.test() to compute 95%-confidence interval.
t.test(body$che_di)
t.test(filter(body, sex == "m")$che_di)
t.test(filter(body, sex == "f")$che_di)
# Ja, vi får fik (optil afrunding) fik vi samme resultat for confidensinterval

## Ex. 2
# 2a
# sample 40 males and 40 females from body
sample_size = 40
sample_m <- tibble(
  sex = "m",
  che_di = sample(filter(body, sex == "m")$che_di, sample_size, replace = FALSE)
)
sample_f <- tibble(
  sex = "f",
  che_di = sample(filter(body, sex == "f")$che_di, sample_size, replace = FALSE)
)

# 2b
# Combine samples of males and females. And make boxplot and histogram
sample_che <- bind_rows(sample_m,sample_f)

sample_che %>% 
  ggplot(aes(x = sex, y = che_di)) +
  geom_boxplot()

sample_che %>%
  ggplot(aes(x = che_di)) +
  geom_histogram(binwidth = 1) +
  facet_wrap(~sex, ncol = 1)

# 2c
# summarise sample_che
sample_summary <- sample_che %>%
  group_by(sex) %>%
  summarise(
    n = n(),
    mean = mean(che_di),
    sd = sd(che_di),
    min = min(che_di),
    q1 = quantile(che_di,0.25),
    median = median(che_di),
    q3 = quantile(che_di,0.75),
    max = max(che_di),
    IQR = IQR(che_di)
  )
sample_summary

# 2d
# two-sample t.test
#t.test(sample_m$che_di, sample_f$che_di)
t.test(sample_che$che_di~sample_che$sex)
# Are conditions fulfilled?
# What are the null hypothesis?
# Is H0 rejected?
# XXX

# 2e
# estimate power wiht power.t.test
power.t.test(delta = 3.85, sd = 2, sig.level = 0.05, n = 40)
# XXX skriv tekst

# 2f
# estimate power wiht power.t.test
power.t.test(delta = 2, sd = 3, sig.level = 0.05, n = 40)
# XXX skriv tekst

## Ex. 3
# 3a
# summarise count of observations and statistical descriptors for body$hgt
hgt_summary <- body %>%
  group_by(sex) %>%
  summarise(
    n = n(),
    mean = mean(hgt),
    sd = sd(hgt),
    min = min(hgt),
    q1 = quantile(hgt,0.25),
    median = median(hgt),
    q3 = quantile(hgt,0.75),
    max = max(hgt),
    IQR = IQR(hgt)
  )
hgt_summary
# plot of body$hgt
body %>%
ggplot(aes(x = sex, y = hgt)) +
  geom_boxplot()
hgt_summary$mean + 2.5 * hgt_summary$sd
# Sample size >> 30 og ingen extrem outliers (2.5 * sd). 
# Male: max = 198 > mean + 2.5 * sd =  196. Therefore assume normal distribution.
# Female: max = 183 > mean + 2.5 * sd = 181
# de er da vist på grænsen af at have extreme outliers XXX

# 3b
# save mean and sd for hgt of males
mu_m <-  hgt_summary$mean[2]
sd_m <- hgt_summary$sd[2]
# plot theoretical normal distribution wiht mean = mu_m og sd = sd_m.
#ggplot(filter(body, sex == "m"), aes(x = hgt)) +
limit_low <- mu_m - 4*sd_m
limit_high <- mu_m + 4*sd_m
ggplot(filter(body, sex == "m"), aes(x = hgt)) +
  geom_histogram(aes(y = ..density..), fill= "darkblue", alpha = 0.1, binwidth = 2) +
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m)) +
  xlim(c(limit_low, limit_high))
# use pnorm() male hgt less than mu_m - 1.95*sd_m
p_low <- pnorm(mu_m - 1.96*sd_m, mean = mu_m, sd = sd_m)
p_low
# use pnorm() male hgt between mu_m +- 1.95*sd_m
p_high <- pnorm(mu_m + 1.96*sd_m, mean = mu_m, sd = sd_m)
p_high - p_low
# Teoretisk er ca. 95 procent sandsynlighed for at hgt ligger mellem +- 1.96 sd_mu fra mean.
# Ikke overraskende ligger 95 % indenfor +-1.96*sd samt 5%/2 = 2.5% ligger lavere end mean - 1.96*sd

# 3c
# same as above for females
mu_f <-  hgt_summary$mean[1]
sd_f <- hgt_summary$sd[1]
# plot theoretical normal distribution wiht mean = mu_f og sd = sd_f.
limit_low <- mu_f - 4*sd_f
limit_high <- mu_f + 4*sd_f
ggplot(filter(body, sex == "f"), aes(x = hgt)) +
  geom_histogram(aes(y = ..density..), fill= "darkred", alpha = 0.1, binwidth = 2) +
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f)) +
  xlim(c(limit_low, limit_high))
# use pnorm() male hgt less than mu_m - 1.95*sd_m
p_low <- pnorm(mu_f - 1.96*sd_f, mean = mu_f, sd = sd_f)
p_low
# use pnorm() male hgt between mu_m +- 1.95*sd_m
p_high <- pnorm(mu_f + 1.96*sd_f, mean = mu_f, sd = sd_f)
p_high - p_low
# Teoretisk er ca. 95 procent sandsynlighed for at hgt ligger mellem +- 1.96 sd_mu fra mean.
# Ikke overraskende ligger 95 % indenfor +-1.96*sd samt 5%/2 = 2.5% ligger lavere end mean - 1.96*sd

# 3d
# count observations in 95%-confidens interval
body %>%
  group_by(sex) %>%
  summarise(
    n = n(),
    mean = mean(hgt),
    sd = sd(hgt),
    n_obs = sum(mean - 1.96*sd <= hgt & hgt <= mean + 1.96*sd),
    p_obs = n_obs/n
  )
# Sammenlignet med teoretisk 95%. Det stemmer fint, både 95% og 95.5% er tæt på de teoretiske 95%.

# 3e
# Plot normal distribution with shaded area
# Males
cutoff_m <- 194
ggplot(filter(body, sex == "m"), aes(x = hgt)) +
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m), xlim = c(cutoff_m, hgt_summary$max[2]), geom = "area", fill = "darkblue", alpha = 0.4) +
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m)) +
  ylab("density")
# use pnorm() to find probability of male hgt > cutoff_m
1 - pnorm(cutoff_m, mean = mu_m, sd = sd_m)

# Females
cutoff_f <- 180
ggplot(filter(body, sex == "f"), aes(x = hgt)) +
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f), xlim = c(cutoff_f, hgt_summary$max[1]), geom = "area", fill = "darkred", alpha = 0.4) +
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f)) +
  ylab("density")
# use pnorm() to find probability of female hgt > cutoff_f
1 - pnorm(cutoff_f, mean = mu_f, sd = sd_f)

# 3f
# plot two normaldistributions like in 3b) and 3c)
# use qnorm() female and male hgt 98%-quantiles
qm <- qnorm(0.98, mean = mu_m, sd = sd_m)
qf <- qnorm(0.98, mean = mu_f, sd = sd_f)
# using 99.7% inside 3*sd as limits
limit_low <- 0.5*(mu_f + mu_m) - 3*sqrt( (sd_f)^2 + (sd_m)^2 )
limit_high <- 0.5*(mu_f + mu_m) + 3*sqrt( (sd_f)^2 + (sd_m)^2 )
hgt_mf <- tibble(hgt = limit_low:limit_high)
ggplot(hgt_mf, aes(x = hgt)) +
  # females
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f), color = "red") +
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f), xlim = c(qf, limit_high), geom = "area", fill = "darkred", alpha = 0.4) +
  # males
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m), color = "blue") +
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m), xlim = c(qm, limit_high), geom = "area", fill = "darkblue", alpha = 0.4) +
  ylab("density")

## Ex. 4
# 4a
body_m <- filter(body, sex == 'm')
ggplot(data = body_m, mapping = aes(x = che_di, y = wgt)) +
 geom_point() +
 geom_smooth(method ="lm")

# 4b
mod <- lm(che_di ~ wgt, data = body_m)
summary(mod)

# Snak om residualer

broom::glance(mod)

# 4c
mod_au <- broom::augment(mod)
mod_au

# 4d
# create scatterplot
mod_au %>%
ggplot(aes(x = wgt, y = .resid)) +
  geom_point()

# 4e
# create boxplot
mod_au %>%
  ggplot(aes(y = .resid, x = "mod_au")) +
  geom_boxplot()

# XXX Skriv tekstsvar her

## Ex. 5
# 5a
# import data from results.csv
results <- read_csv("results.csv", col_types = cols(date = col_date(format = "%Y-%m-%d")))
summary(results)
results

# 5b
# filter out some
UEFI <- results %>%
  filter(neutral == FALSE) %>%
  #filter(tournament == "UEFA Euro qualification" | tournament == "FIFA World Cup qualification") %>%
  filter(tournament %in% c("UEFA Euro qualification", "FIFA World Cup qualification")) %>%
  filter(date >= "1990-01-01")
UEFI

# 5c
# mutate and create UEFI_home
UEFI_home <- UEFI %>%
  mutate(score = home_score - away_score,
         team = home_team,
         type = "home")
UEFI_home

# 5d
# mutate and create UEFI_away
UEFI_away <- UEFI %>%
  mutate(score = away_score - home_score, 
         team = away_team, 
         type = "away"
         )
UEFI_away

# 5e
# combine UEFI_home og UEFI_away into UEFI_games
UEFI_games <- rbind(UEFI_away, UEFI_home)
UEFI_sample <- filter(UEFI_games, -6 <= score & score <= 6,
                      team %in% c("Denmark", "Iceland", "Spain", "Germany", "Northern Ireland", "Faroe Islands", "Malta", "Italy", "France", "England", "Scotland", "Norway", "Portugal", "Belgium", "Greece", "Sweden"))
UEFI_sample

ggplot(data = UEFI_sample, mapping = aes(x = type, y = score)) +
 geom_boxplot()

ggplot(data = UEFI_sample, mapping = aes(x = score, fill = type)) +
 geom_bar(position = "dodge")
# SVAR:
# Både på boxplot og søjlediagram ses at hjemmehold er forskudt mod højere antal mål end udehold. Dette viser at hjemmehold (i middel) scorer flere mål end udehold. Dette kan indikere at Uefa/Fifa-fodboldkampe oftere ender med hjemmesejre end med udesejr.

# 5f
type_counts <- UEFI_sample %>%
  group_by(type) %>%
  summarise(
    loose = sum(score < 0),
    draw = sum(score == 0),
    win = sum(score > 0)
  )
type_counts
#View(UEFI_sample %>% filter(date == "1990-10-10"))
# add totals (marginaler)
type_totals <- type_counts %>% 
  adorn_totals("row") %>% 
  adorn_totals("col")
tibble(type_totals)
# add percentages
type_percent <- type_totals %>% 
  adorn_percentages(denominator = "col")
tibble(type_percent)

# Beregner forventede værdier. Fjerner den første kolonne (type) og piper til chisq.test()
addmargins(
  chisq.test(type_counts %>% select(-type))$expected
  )
# SVAR: Tekst om betingelser se .Rmd
# chisq.test
chisq.test(type_counts[,-1])

# lav boxplot
ggplot(data = UEFI_sample, mapping = aes(x = fct_reorder(team, score, .fun = median, .desc = FALSE), y = score)) +
  geom_boxplot() +
  xlab("team") +
  coord_flip()
# boxplot med facet_wrap
ggplot(data = UEFI_sample, mapping = aes(x = type, y = score)) +
  geom_boxplot() +
  facet_wrap(~team)
# create team_counts
team_counts <- UEFI_sample %>% 
  group_by(team, type) %>% 
  summarise(
    loose = sum(score < 0),
    draw = sum(score == 0),
    win  = sum(score > 0),
    .groups = 'drop'
  )
team_counts
# chisq.test of indenpendense away/home vs. loose/draw/win for Faroe Islads
fo <- team_counts %>%
  filter(team == "Faroe Islands")
chisq.test(fo %>% select(-team, -type))

addmargins(
  chisq.test(fo %>% select(-team, -type))$expected
)
# SVAR: H0 er at der er uafhængighed mellem away/home vs. loose/draw/win for Faroe Islads.
# med en p-værdi = 0.20 > 0.05 kan vi ikke afvise H0.
# Det er faktisk rimeligt at lave chisq.test for Færøerne, da alle celler har mindst 5 forventede tilfælde.

# chisq.test of indenpendense away/home vs. loose/draw/win for Iceland
ice <- team_counts %>%
  filter(team == "Iceland")
ice
chisq.test(ice %>% select(-team, -type))

addmargins(
  chisq.test(ice %>% select(-team, -type))$expected
)
# SVAR:
#    Nul-hypotesen er at der er uafhængighed mellem away/home vs. loose/draw/win for Iceland.
#    Med en p-værdi = 0.002 < 0.05 kan vi på et 5%-signifikansniveua afvise H0.
#    Med andre ord: For Island er en sammenhæng mellem ude-/hjemmebane og kampens udfald.
#    Det er rimeligt at lave chisq.test for Iceland, da alle celler har forventede værdier på mindst 5.