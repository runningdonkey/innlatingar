---
title: "5021.20 - kravd innlating 3 (required lab 3)"
author: "Heini Vatnsdal og Pauli Baadsager"
date: 09-10-2020
output: html_notebook
---

## Data Science and statistics with RStudio

### Introduction

> Similar requirements as for the first and second required assignment (see the previous exercises).

```{r}
# Put your answers to code-based questions into the given empty code-blocks. 

# Run (test) your individual code chunks (Ctrl + Shift + Enter).

# Run All code blocks (Ctrl + Alt + R) before you "Preview" (Ctrl + Shift + K) the notebook in a browser.
```

    By starting a text-line with 4 empty spaces, format your text-based answers into a box right below such questions.

> When finished, run all code blocks (Ctrl + Alt + R) and make a final "Preview" (Ctrl + Shift + K), to be opened in a browser and hand in the document: "kravd3_5021_NN1_NN2.nb.html".


### Load packages

> In this exercise, you will need to load the packages: `tidyverse`, `openintro`, `broom`, `lubridate`, `janitor`.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
# you can load your packages here
```

```{r include = FALSE}
library(tidyverse)
library(openintro)
library(broom)
library(lubridate)
library(janitor)

```


### Exercise 1

> a) show the output of `bdims`.
- how many observations and variables does it contain?

```{r}
bdims
```
    bdims hava 507 observationer og 25 variablar.

> b) investigate the structure `str()` of `bdims`:
- does it contain any factor variables?

    Nei, bdims inniheldur ongar 'factors'.

```{r}
str(bdims)
```

> c) 
- create tibble `body` as a `mutate` of `bdims` in which you change variable `sex` to factor with use of `factor()` with `levels = c(0, 1)`, and recode the factor levels of `sex` to `f` and `m` instead of `0` and `1` (tip: use fct_recode()).

```{r}
is_tibble(bdims)
body <- bdims %>% 
  mutate(
    sex = factor(sex, levels=c(0,1)),
    sex = fct_recode(sex, f="0", m="1")
  )
```


> d) 
- use `select()` to only select the three variables `age`, `hgt` and `sex` from `body`. Save the result in `body3` and output `body3`.
- then, use `n_distinct()` and `distinct()` to respectively count and list the distinct records in `body3`.

```{r}
body3 <- select(body, age, hgt, sex)
body3

n_distinct(body3,na.rm=TRUE)
distinct(body3)
```

> e) now use `body` with the recoded factor `sex` you made in c), and make these two plots:
- a `geom_boxplot` for `x = sex, y = che_di`.
- a `geom_histogram` for `x = che_di` using a binwidth of `1` in a vertically stacked facet-plot by variable `sex`.

```{r}
ggplot(data = body, mapping = aes(x = sex, y = che_di)) +
 geom_boxplot()

ggplot(data = body, mapping = aes(x = che_di)) +
 geom_histogram(binwidth = 1) + 
  facet_wrap(~ sex, ncol = 1)
```

> f) now make a pipe of `body` that is `grouped_by` variable `sex` to `summarise` the number `n` of observations and to compute for variable `che_di`: the `mean`, `sd`, `min`, `q1`, `median`, `q3`, `max` and `IQR`. Your result should be 10 columns of variables (`sex`, `n`, `mean`, `sd`, `min`, `q1`, `median`, `q3`, `max` and `IQR`) with 2 rows of summarised observations - one row for each factor level of variable `sex`. Save the results of the pipe in tibble `che_summary` and output `che_summary`.

```{r, message=FALSE}
che_summary <- body %>%
  group_by(sex) %>%
  summarise(
    n = n(),
    mean = mean(che_di),
    sd = sd(che_di),
    min = min(che_di),
    q1 = quantile(che_di, 0.25),
    meadian = median(che_di),
    q3 = quantile(che_di, 0.75),
    max = max(che_di),
    IQR = IQR(che_di)
  )
che_summary
```

> 
- consider the two conditions (Independency, Normality)-conditions required to apply the Central Limit Theorem for The Sample Mean. Are these conditions (resonably) fulfilled for the sampling distribution of the mean `che_di` in `body` of females and males, respectively? 

    Independency er uppfylt tá observatiónirnar eru tilvildarligar fyri menn og kvinnur. "f" og "m" skulu eisini vera óheft.
    
    
> based on t-confidence intervals for the mean, compute the 95%-confidence interval for the mean `che_di` of females and males in the sample `body` by using a pipe of `che_summary` to a `select()` of variables `sex`, `n`, `mean`, `sd` and then a `mutate()` to add the computed variables `t_df`, `SE`, `c_low` and `c_high` (Tip: t_df = qt(0.975, n-1)).

```{r}
z_95 <- 1.96
che_summary %>% 
  select(sex, n, mean, sd) %>%
  mutate(
    t_df = qt(0.975, n - 1),
    SE = sd/sqrt(n),
    c_low = mean - z_95*SE, 
    c_high = mean + z_95*SE
  )
```

> 
- also, use a one-sample `t.test()` to compute the 95%-confidence interval for the mean `che_di` of females and males in the sample `body`.
- compare the confidence intervals from the `t.test()` with your own calculation of these confidence intervals for males and females, respectivley. Do they agree?

```{r}
t.test(body$che_di)
t.test(filter(body, sex == "m")$che_di)
t.test(filter(body, sex == "f")$che_di)
```
    Ja, for både kvinder og mænd fik vi samme resultat for konfidens intervaller (op til afrunding med to decimaler).


### Exercise 2

> a) use the `sample()` function to sample from `body` 40 values of `che_di` for females and 40 values of `che_di` for males. Save your result in two tibbles `sample_m` and `sample_f` for males and females, respectively, whith two variables `sex` and `che_di` (tip: you can set `sex = m` and `sex = f` inside the tibble definitions). 

```{r}
sample_size = 40
sample_f <- tibble(
  sex = "f",
  che_di = sample(filter(body, sex == "f")$che_di, sample_size, replace = FALSE)
)
sample_m <- tibble(
  sex = "m",
  che_di = sample(filter(body, sex == "m")$che_di, sample_size, replace = FALSE)
)

```

> b) combine tibble `sample_m` and `sample_f` into one tibble `sample_che` and as previously make: 
- a `geom_boxplot` for `x = sex, y = che_di`.
- a `geom_histogram` for `x = che_di` using a binwidth of `1` in a vertically stacked facet-plot by variable `sex`.

```{r}
sample_che <- bind_rows(sample_m, sample_f)
sample_che

ggplot(data = sample_che, mapping = aes(x = sex, y = che_di)) +
 geom_boxplot()

ggplot(data = sample_che, mapping = aes(x = che_di)) +
 geom_histogram(binwidth = 1) + 
  facet_wrap(~ sex, ncol = 1)
```

> c)  now make a pipe of `sample_che` that is `grouped_by` variable `sex` to `summarise` the number `n` of observations and to compute for variable `che_di`: the `mean`, `sd`, `min`, `q1`, `median`, `q3`, `max` and `IQR`. Your result should be 10 columns of variables (`sex`, `n`, `mean`, `sd`, `min`, `q1`, `median`, `q3`, `max` and `IQR`) with 2 rows of summarised observations - one row for each factor level of variable `sex`. Save the results of the pipe in tibble `sample_summary` and output `sample_summary`.

```{r, message=FALSE}
sample_summary <- sample_che %>%
  group_by(sex) %>%
  summarise(

 n = n(),
 mean = mean(che_di),
 sd = sd(che_di),
 min = min(che_di),
 q1 = quantile(che_di, 0.25),
 meadian = median(che_di),
 q3 = quantile(che_di, 0.75),
 max = max(che_di),
 IQR = IQR(che_di)
 
)
sample_summary
```

> d) compute the estimate and 95%-confidence interval for the difference in `che_di` in `sample_m` and `sample_f` between males and females using a two-sample `t.test()`. 
- are the conditions for using this test fulfilled? What is the null hypothesis of the test? Is it rejected?

    H0 er at eingin munur er millum menn og kvinnur tá tað kemur til chest dimensions. Vit kunnu tí forkasta H0 tá p virðið < 0.05. Treytirnar fyri at útføra t-test eru í ordan.

```{r}
t.test(sample_che$che_di~sample_che$sex)
```

> e) estimate the power of your test to detect a difference between means using `power.t.test()` assuming a minimum effect size `delta = 3.85, sd = 2, sig.level = 0.05, n = 40`.

```{r}
power.t.test(delta = 3.85, sd = 2, sig.level = 0.05, n = 40)
```

> f) estimate the power of a two-sample t-test to detect a difference between means using `power.t.test()` assuming a minimum effect size `delta = 2, sd = 3, sig.level = 0.05, n = 40`.

```{r}
power.t.test(delta = 2, sd = 3, sig.level = 0.05, n = 40)
```

### Exercise 3

> a)  now make a pipe of `body` that is `grouped_by` variable `sex` to `summarise` the number `n` of observations and to compute for variable `hgt`: the `mean`, `sd`, `min`, `q1`, `median`, `q3`, `max` and `IQR`. Your result should be 10 columns of variables (`sex`, `n`, `mean`, `sd`, `min`, `q1`, `median`, `q3`, `max` and `IQR`) with 2 rows of summarised observations - one row for each factor level of variable `sex`. Save the results of the pipe in tibble `hgt_summary` and output `hgt_summary`.

```{r, message=FALSE}
hgt_summary <- body %>%
  group_by(sex) %>%
  summarise(

 n = n(),
 mean = mean(hgt),
 sd = sd(hgt),
 min = min(hgt),
 q1 = quantile(hgt, 0.25),
 meadian = median(hgt),
 q3 = quantile(hgt, 0.75),
 max = max(hgt),
 IQR = IQR(hgt)
 
)
hgt_summary
```
>
- also make a `geom_boxplot` for `x = sex, y = hgt` in `body`.
- from the output of `hgt_summary` and the the boxplots confirm if the distributions of `hgt` can be assumed to be normally distributed? 

    Ja, median og mean hava sama virðið. Eisini sæst á boksplottunum at mean er í miðjuni á IQR.

```{r}
ggplot(data = body, mapping = aes(x = sex, y = hgt)) +
  geom_boxplot()
hgt_summary$mean + 2.5 * hgt_summary$sd
```

    Sample size >> 30 og ingen extrem outliers (2.5 * sd). Therefore okay to assume normal distribution.
    Male: max = 198 > mean + 2.5 * sd =  196. 
    Female: max = 183 > mean + 2.5 * sd = 181.
    
    Der er outliers, men ikke extreme outliers.

> b)  extract for males the `mean` and `sd` for variable `hgt` in `hgt_summary` and save them in variables `mu_m` and `sd_m`.
- assuming that `hgt` of males is normally distributed with `mu_m` and `sd_m`, plot a theoretical normal distribution curve for variable `hgt` using `ggplot` and a  `stat_function` (tip: make tibble `hgt_m` with one variable `hgt` and use `mu_m - 4*sd_m` and `mu_m + 4*sd_m` as the lower and upper limits for the range of `hgt`). 
- use `pnorm()` to find the theoretical probabilities of:
- `hgt` for males being less than `mu_m - 1.96*sd_m`.
- `hgt` for males being between `mu_m - 1.96*sd_m` and `mu_m + 1.96*sd_m`.

```{r}
mu_m <-  hgt_summary$mean[2]
sd_m <- hgt_summary$sd[2]
# plot theoretical normal distribution wiht mean = mu_m og sd = sd_m, using the tip.
limit_low <- mu_m - 4*sd_m
limit_high <- mu_m + 4*sd_m
hgt_m <- tibble(hgt = limit_low:limit_high)
ggplot(filter(body, sex == "m"), aes(x = hgt)) +
  geom_histogram(aes(y = ..density..), fill= "darkblue", alpha = 0.1, binwidth = 2) +
  stat_function(data = hgt_m, fun = dnorm, args = list(mean = mu_m, sd = sd_m))
# use pnorm() male hgt less than mu_m - 1.95*sd_m
p_low <- pnorm(mu_m - 1.96*sd_m, mean = mu_m, sd = sd_m)
p_low
# use pnorm() male hgt between mu_m +- 1.95*sd_m
p_high <- pnorm(mu_m + 1.96*sd_m, mean = mu_m, sd = sd_m)
p_high - p_low

```
    
    Teoretisk er ca. 95 procent sandsynlighed for at hgt ligger mellem mean +- 1.96 sd_mu. Ikke overraskende ligger 95% indenfor +-1.96*sd samt 5%/2 = 2.5% ligger lavere end mean - 1.96*sd

> c) similarly, extract for females the `mean` and `sd` for variable `hgt` in `hgt_summary` and save them in variables `mu_f` and `sd_f`.
- assuming that `hgt` of females is normally distributed with `mu_f` and `sd_f`, plot a theoretical normal distribution curve for variable `hgt` using `ggplot` and a `stat_function` (tip: make tibble `hgt_f` with one variable `hgt` and use `mu_f - 4*sd_f` and `mu_f + 4*sd_f` as the lower and upper limits for the range of `hgt`). 
- use `pnorm()` to find the theoretical probabilities of:
- `hgt` for females being less than `mu_f - 1.96*sd_f`.
- `hgt` for females being between `mu_f - 1.96*sd_f` and `mu_f + 1.96*sd_f`.

```{r}
mu_f <-  hgt_summary$mean[1]
sd_f <- hgt_summary$sd[1]
# plot theoretical normal distribution wiht mean = mu_f og sd = sd_f.
limit_low <- mu_f - 4*sd_f
limit_high <- mu_f + 4*sd_f
hgt_f <- tibble(hgt = limit_low:limit_high)
ggplot(filter(body, sex == "f"), aes(x = hgt)) +
  geom_histogram(aes(y = ..density..), fill = "darkred", alpha = 0.1, binwidth = 2) +
  stat_function(data = hgt_f, fun = dnorm, args = list(mean = mu_f, sd = sd_f))
# use pnorm() male hgt less than mu_m - 1.95*sd_m
p_low <- pnorm(mu_f - 1.96*sd_f, mean = mu_f, sd = sd_f)
p_low
# use pnorm() male hgt between mu_m +- 1.95*sd_m
p_high <- pnorm(mu_f + 1.96*sd_f, mean = mu_f, sd = sd_f)
p_high - p_low
```

    Teoretisk er ca. 95 procent sandsynlighed for at hgt ligger mellem mean +- 1.96 sd_mu. Ikke overraskende ligger 95% indenfor +-1.96*sd samt 5%/2 = 2.5% ligger lavere end mean - 1.96*sd


> d) not using the theoretical normal distributions and `pnorm()`, find the observed probability of the variable `hgt` being between `mu` $\pm$ `1.96 * sd` by counting the relevant observations for each `sex`:
- make one pipe of `body` grouped by variable `sex` to summarise the number `n` of observations and to compute for variable `hgt`: the `mean`, `sd`, the number `n_obs` of observations of `hgt` between `mean - 1.96 * sd` and `mean + 1.96 * sd`, and the probability `p_obs = n_obs/n`. Your results should be a tibble with two rows of results for the variables `sex`, `n`, `mean`, `sd`, `n_obs` and `p_obs`.
- compare your results for `p_obs` with the corresponding theoretical results from b) and c), are they similar?

```{r, message=FALSE}
body %>%
  group_by(sex) %>%
  summarise(
    n = n(),
    mean = mean(hgt),
    sd = sd(hgt),
    n_obs = sum(mean - 1.96*sd <= hgt & hgt <= mean + 1.96*sd),
    p_obs = n_obs/n
  )
```
    
    Sammenlignet med teoretisk 95%. Det stemmer fint, både 95% og 95.5% er tæt på de teoretiske 95%.

> e) 
- plot the normal distribution with a shaded area corresponding to the probability that the height of males is higher than `194` cm (tip: use two `stat_function` one for the curve, and one for the area controlled by `xlim = c(low_limit, high_limit)` and `geom = "area"`).
- use `pnorm()` to find the probability.

```{r}
# Males
cutoff_m <- 194
limit_high <- mu_m + 4*sd_m
ggplot(filter(body, sex == "m"), aes(x = hgt)) +
  stat_function(data = hgt_m,
                fun = dnorm, args = list(mean = mu_m, sd = sd_m),
                xlim = c(cutoff_m, limit_high),
                geom = "area",
                fill = "darkblue",
                alpha = 0.4) +
  stat_function(data = hgt_m, 
                fun = dnorm, args = list(mean = mu_m, sd = sd_m)) +
  ylab("density")
# use pnorm() to find probability of male hgt > cutoff_m
1 - pnorm(cutoff_m, mean = mu_m, sd = sd_m)
```

> 
- plot the normal distribution with a shaded area corresponding to the probability that the height of females is higher than `180` cm (tip: use two `stat_function` one for the curve, and one for the area controlled by `xlim = c(low_limit, high_limit)` and `geom = "area"`).
- use `pnorm()` to find the probability.

```{r}
# Females
cutoff_f <- 180
limit_high <- mu_f + 4*sd_f
ggplot(filter(body, sex == "f"), aes(x = hgt)) +
  stat_function(data = hgt_f,
                fun = dnorm, args = list(mean = mu_f, sd = sd_f),
                xlim = c(cutoff_f, limit_high),
                geom = "area",
                fill = "darkred",
                alpha = 0.4) +
  stat_function(data = hgt_f, 
                fun = dnorm, args = list(mean = mu_f, sd = sd_f)) +
  ylab("density")
# use pnorm() to find probability of female hgt > cutoff_f
1 - pnorm(cutoff_f, mean = mu_f, sd = sd_f)
```

> f) plot the two normal distributions from b) and c) in one plot using `ggplot` and two `stat_function` (tip: make tibble `hgt_mf` with one variable `hgt` and a suitable range to use so both curves are fully visible). Colour the normal distribution curve red for females and blue for males.

```{r}
# using 99.7% inside 3*sd as limits
limit_low <- 0.5*(mu_f + mu_m) - 3*sqrt( (sd_f)^2 + (sd_m)^2 )
limit_high <- 0.5*(mu_f + mu_m) + 3*sqrt( (sd_f)^2 + (sd_m)^2 )
hgt_mf <- tibble(hgt = limit_low:limit_high)
ggplot(hgt_mf, aes(x = hgt)) +
  # Females
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f), color = "red") +
  # Males
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m), color = "blue") +
  ylab("density")
```

>
- use `qnorm` to find the 98% quantiles for the heights of males and females, respectively, save them in `qm` and `qf` and list the output.
- add shaded areas to your plot with the two normal distributions for the heights of males and females corresponding to the 2% probability of heights larger than the 98% quantiles and colour the areas red for females and blue for males.

```{r}
qf <- qnorm(0.98, mean = mu_f, sd = sd_f)
qm <- qnorm(0.98, mean = mu_m, sd = sd_m)
qf
qm
# using 99.7% inside 3*sd as limits
ggplot(hgt_mf, aes(x = hgt)) +
  # Females
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f ), color = "red") +
  stat_function(fun = dnorm, args = list(mean = mu_f, sd = sd_f), xlim = c(qf, limit_high), geom = "area", fill = "darkred", alpha = 0.4) +
  # Males
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m), color = "blue") +
  stat_function(fun = dnorm, args = list(mean = mu_m, sd = sd_m), xlim = c(qm, limit_high), geom = "area", fill = "darkblue", alpha = 0.4) +
  ylab("density")
```

### Exercise 4

> a) create a tibble `body_m` by filtering all the observations for males from `body`.
- make one `geom_point` scatterplot with an added linear line from `geom_smooth` for `wgt` versus `che_di` (`x = che_di, y = wgt`) in `body_m`.

```{r}
body_m <- filter(body, sex == "m")
body_m

ggplot(data = body_m, mapping = aes(x = che_di, y = wgt)) +
  geom_point() +
  geom_smooth(method ="lm")
```

> b) run the code shown below to create a model object `mod` for the linear model of males and to output the model summary.

```{r}
mod <- lm(che_di ~ wgt, data = body_m)
summary(mod)
```

> 
- The “Residuals:” in the summary-output can be used as a fast check for the distribution assumption.
>
- does the “Residuals:” section in this case indicate any problems? Explain your reasoning.

    Residuals vísir ikki nakrar stórar trupulleikar. Hyggja vit eftir tølunum síggja vit at fordeilingin er nøkunlunda symmertrisk, tvs. at median er tætt uppá 0 og at 1. og 3. kvartil hava líknandi virðir.
    Tó síggja vit kanska eitt hall, sum er størri enn null fyri hallparametrið av residualini. Så vi må være lidt på udkig efter tendens til af afvige fra en lineær model. Ved lave værdier af "che_di" er lille overvægt af punkter med "wgt" lavere end modellen, mens for høje værdier af "che_di" er en lille overvægt af punkter med "wgt" større end modellen.

> finally, show the quality-parameters of `mod` in a tidy format by using `broom::glance`.
> describe the meaning of `r.squared` and the `p.value` in the output.

    r.squared - Tað sæst at 53% av variansinum er eftirvíst av modellinum.
    p.value - Tað sæst at p virðið er lægri enn 0.05. Tí kunnu vit forkasta H0 ið sigur at eingin samanhangur er millum wgt og chest dim.

```{r}
broom::glance(mod)
```

> c) create `mod_au` to add model residuals, predictions etc. for each data point for the model `mod` by using `bromm::augment` (tip: use `mod` as the only argument, to only show relevant model data). List the output of `mod_au`.

```{r}
mod_au <- broom::augment(mod)
mod_au
```

> d) create:
 - a `geom_point` scatterplot of `x = wgt` and `y = .resid` from `mod_au` to get a residuals-plot.
 
```{r}
ggplot(data = mod_au) +
 geom_point(mapping = aes(x = wgt, y = .resid))
```

> e) create:
- a `geom_boxplot` of `y = .resid` from `mod_au` to get a residuals-boxplot (tip: use `x = "mod_au"`).

```{r}
ggplot(data = mod_au, mapping = aes(x = "mod_au", y = .resid)) +
 geom_boxplot()
```

> f) based on your "Residuals:" section in b), the plots in d) and e) and the sample size, is the model-assumption for the residuals (reasonably) fulfilled?


### Exercise 5

> a) import the provided file of football data `results.csv` into the object `results` by using `read_csv` while you set the column format of the column date to `col_types = cols(date = col_date(format = "%Y-%m-%d"))`. 
- when imported, output `results`.

```{r}
results <- read_csv("results.csv", col_types = cols(date = col_date(format = "%Y-%m-%d")))
results
```

> b) save in `UEFI` a filter of `results` to only include:
- `neutral` being `FALSE` to exclude games on neutral ground.
- `tournament` "UEFA Euro qualification", "FIFA World Cup qualification".
- `date` larger or equal than "1990-01-01".
- output `UEFI`.

```{r}
UEFI <- results %>%
  filter(neutral == FALSE) %>%
  filter(tournament %in% c("UEFA Euro qualification", "FIFA World Cup qualification")) %>%
  filter(date >= "1990-01-01")
UEFI
```

> c) save in object `UEFI_home` a pipe of `UEFI` through a mutate to:
- add the columns `score = home_score - away_score`, `team = home_team` and `type = "home"`. 
- output `UEFI_home`.

```{r}
UEFI_home <- UEFI %>%
  mutate(score = home_score - away_score,
         team = home_team,
         type = "home")
UEFI_home
```

> d)  similarly, save in object `UEFI_away` a pipe of `UEFI` to:
- add the columns `score = away_score - home_score`, `team = away_team` and `type = "away"`. 
- output `UEFI_away`.

```{r}
UEFI_away <- UEFI %>%
  mutate(score = away_score - home_score,
         team = away_team,
         type = "away")
UEFI_away
```

> e) use `rbind()` to combine `UEFI_away` and `UEFI_home` into `UEFI_games`, then:
- filter `UEFI_games` into `UEFI_sample` to include:
- `score` between -6 and 6 (both inclusive).
- `team` "Denmark", "Iceland", "Spain", "Germany", "Northern Ireland", "Faroe Islands", "Malta", "Italy", "France", "England", "Scotland", "Norway", "Portugal", "Belgium", "Greece", "Sweden".
- make a `geom_boxplot` of `UEFI_sample` with `x = type, y = score`.
- make a `geom_bar` plot of `UEFI_sample` with `x = score` in which you set `fill = type` and `position = dodge`.
- for the teams in `UEFI_sample`, which effect do these two plots potentially indicate?

```{r}
UEFI_games <- rbind(UEFI_away, UEFI_home)
UEFI_sample <- filter(UEFI_games, -6 <= score & score <= 6,
                      team %in% c("Denmark", "Iceland", "Spain", "Germany", "Northern Ireland", "Faroe Islands", "Malta", "Italy", "France", "England", "Scotland", "Norway", "Portugal", "Belgium", "Greece", "Sweden"))
UEFI_sample

ggplot(data = UEFI_sample, mapping = aes(x = type, y = score)) +
 geom_boxplot()

ggplot(data = UEFI_sample, mapping = aes(x = score, fill = type)) +
 geom_bar(position = "dodge")
```

    Både på boxplot og søjlediagram ses at hjemmehold er forskudt mod højere antal mål end udehold. Dette viser at hjemmehold (i middel) scorer flere mål end udehold. Dette kan indikere at Uefa/Fifa-fodboldkampe oftere ender med hjemmesejre end med udesejr.

> f) save in `type_counts` a pipe of `UEFI_sample` grouped by `type` to summarise: `loose` (score < 0), ` draw` (score == 0) and `win` (score > 0). Your result should contain 4 columns: `type`, `loose`, `draw`, `win` with two rows of counts (one row for each `type`). Output `type_counts`.

```{r}
type_counts <- UEFI_sample %>%
  group_by(type) %>%
  summarise(
            loose = sum(score < 0),
            draw = sum(score == 0),
            win  = sum(score > 0),
            .groups = 'drop'
            )
type_counts
```

> 
- add row and column totals by piping `type_counts` to `adorn_totals()` and save your result in `type_totals`. Output `type_totals` by using `tibble(type_totals)`. Your result should contain 5 columns `type`, `loose`, `draw`, `win`, `Total` and 3 rows where the last row is the column totals.

```{r}
type_totals <- type_counts %>% 
  adorn_totals("row") %>% 
  adorn_totals("col")
tibble(type_totals)
```

> 
- add percentages with respect to the column totals by piping `type_totals` to `adorn_percentages()` and save your result in `type_percent`. Output `type_percent` by using `tibble(type_percent)`.

```{r}
type_percent <- type_totals %>% 
  adorn_percentages(denominator = "col")
tibble(type_percent)
```
> 
- do you think the variables `loose`, `draw`, `win` in `type_counts` are dependent or independent of the variable `type`? Explain your reasoning. 

    Hvis fodboldkampenes udfald (loose/draw/win) er uafhængigt af ude-/hjemmebane, så må forventes at både loose, draw og win er fordelt med 50% til hjemmeholdet og 50% til udeholdet. Dette er tydeligvis ikke tilfældet i tabellen ovenfor. 
    På lignende vis kan vi sammenligne de forventede værdier (og ikke kun procentfordelingen)
    
```{r}    
# Forventede værdier beregnes (med chisq.test)
addmargins(
  chisq.test(type_counts %>% select(-type))$expected
  )
```

    Teir eru heftir at variablinum "type". Tá broytingar eru í "type" sæst somuleiðis, at ein markant broyting er í "win" og "loose" hjá "home" og "away".
    Ligesom ved sammenligningen af procentfordelingen ser vi her, at når forventede værdier sammenlignes med observerede værdier (type_totals), vinder hjemmehold mere end forventet (obs: 637 / forv: 560) og líka fyri úti-hold tey tabur meira enn vantað (obs: 372 / forv: 310).
    
> by using `type_counts` and `chisq.test()`, test the null hypothesis of independence. What is the conclusion of the test and does the conclusion agree with your own reasoning (above)?

> investigate the conditions for using the `chisq.test()`, was is OK to apply this test here?

```{r}
chisq.test(type_counts[,-1])
```

    Útfrá testinum sæst at p-virðið = 3.4E-11 < 0.05. Tí kunnu vit forkasta H0 um at "type" variabulin er óheftur at "loose", "draw" og "win". Hetta er eisini sama niðurstøða, sum er nevnd omanfyri.

    Alle celler har mindst 5 forventede observationer.
    Derudover gælder for ethvert hold i {away, home} at holdet IKKE er både ude- og hjemmehold, samt at holdet tilhører netop en af kategorierne {loose, draw, win}.
    Betingelserne for at benytte Chi-squaretest er opfyldt. 

> 
- make a `geom_boxplot` of `UEFI_sample` for `y = score` in which you use `x = fct_reorder()` to order `team` by increasing values of `score` (default, the median score) and finally use `coord_flip()` to flip the `x`- and `y`-axis to get more space to show the `team` variable.
- make a `geom_boxplot` of `UEFI_sample` for `x = type, y = score` that you `facet_wrap` by variable `team`.
- comment on these two plots, what are the main effects that you observe here?

```{r}
ggplot(data = UEFI_sample, 
       mapping = aes(x = fct_reorder(team, score, .fun = median, .desc = FALSE), y = score)) +
  geom_boxplot() +
  xlab("team") +
  coord_flip()
# boxplot med facet_wrap
ggplot(data = UEFI_sample, mapping = aes(x = type, y = score)) +
  geom_boxplot() +
  facet_wrap(~team)
```
> 
- save in `team_counts` a pipe of `UEFI_sample` grouped by `team` and `type` to summarise: `loose` (score < 0), ` draw` (score == 0) and `win` (score > 0). Your result should contain 5 columns: `team`, `type`, `loose`, `draw`, `win` with two rows of counts for each `team` (one row for each `type`). Output `team_counts`.

```{r}
team_counts <- UEFI_sample %>% 
  group_by(team, type) %>% 
  summarise(
    loose = sum(score < 0),
    draw = sum(score == 0),
    win  = sum(score > 0),
    .groups = 'drop'
  )
team_counts
```

> 
- by using `team_counts` and `chisq.test()`, test the null hypothesis of independence between the `type` variable (home and away) and the counts of `loose`, `draw` or `win` for Faroe Islands. 
- what is the conclusion of the test?
- investigate the conditions for using the `chisq.test()`, was is OK to apply this test for the Faroe Islands?

```{r}
# chisq.test of indenpendense away/home vs. loose/draw/win for Faroe Islands
fo <- team_counts %>%
  filter(team == "Faroe Islands")
chisq.test(fo %>% select(-team, -type))
# Forventede værdier
addmargins(
  chisq.test(fo %>% select(-team, -type))$expected
)
```


    Nul-hypotesen er at der er uafhængighed mellem away/home vs. loose/draw/win for Faroe Islads.
    Med en p-værdi = 0.20 > 0.05 kan vi ikke afvise H0. 
    Det er faktisk rimeligt at lave chisq.test for Færøerne, da alle celler har mindst 5 forventede tilfælde.


> 
- similarly, by using `team_counts` and `chisq.test()`, test the null hypothesis of independence between the `type` variable (home and away) and the counts of `loose`, `draw` or `win` for Iceland. 
- what is the conclusion of the test?
- investigate the conditions for using the `chisq.test()`, was is OK to apply this test for Iceland?

```{r}
ice <- team_counts %>%
  filter(team == "Iceland")
ice
chisq.test(ice %>% select(-team, -type))
# Forventede værdier
addmargins(
  chisq.test(ice %>% select(-team, -type))$expected
)
```

    Nul-hypotesen er at der er uafhængighed mellem away/home vs. loose/draw/win for Iceland.
    Med en p-værdi = 0.002 < 0.05 kan vi på et 5%-signifikansniveua afvise H0.
    Med andre ord: For Island er en sammenhæng mellem ude-/hjemmebane og kampens udfald.
    Det er rimeligt at lave chisq.test for Iceland, da alle celler har forventede værdier på mindst 5.
    
    Ved sammenligning af tabellerne for hhv. observationer og forventede værdier (under H0), se at Island er bedre til at vinde deres fodboldkampe når de er på hjemmebane (obs: 33 / forv: 24) end når de er på udebane (obs: 15 / forv: 24).
