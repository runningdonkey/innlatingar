---
title: "Kravd innlating 1"
author: "Pauli Baadsager."
date: "Innlating í Moodle: seinast um kvøldið 8. september 2020."
output: html_notebook
---

## Dátuvísindi og hagfrøði við RStudio

### Innleiðing

> Hendan innlating skal skal gerast í RStudio við brúk av R Markdown. Hetta soleiðis, at tú brúkar hugtøkini, sum vit hava lært innan endurskapandi gransking (reproducible research) við RStudio, har tú bindur tín tekst, R-kodu og úrslit saman í eitt html_notebook skjal, sum tú síðani letur inn. 
>
> Hevur tú trupulleikar við at fáa R Markdown at virka, ella bert vilt skilja R Markdown betur, so les meira um evnið í _R for Data Science_ lærubókini (__27__ R Markdown og __29__ R Markdown formats).
>
> Henda innlatingin er einamest um greining og plot av data úr _OpenIntro Statistics_ hagfrøði-lærubókini (4. útgáva) í skeiðnum (pakki `openintro`). Hetta við brúk av `tidyverse` hentleikum, sum tit hava lært í _R for Data science_ lærubókini, umframt eyka hentleikum úr pakka `janitor`.

> Arbeiðið saman - helst bólkar á tvey lesandi - tó undantøk kunna gerast. Tað er sera umráðandi, at hvør einstaklingur í bólkinum er væl við og ger sín part av arbeiðinum. Innlatingin skal vera testað og virka á teldunum hjá hvørjum einstøkum limi í bólkinum.

> Tú/tit opna hetta skjal í RStudio, sum ein template til innlatingina, og skriva tykkara R-kodu í teir givnu, men tómu kodublokkarnar.

> Teir spurningar, sum tit skula svara við vanligum teksti ella tølum, skula tit gera við at svara beint undir hvørjum slíkum spurningi, á linju tit byrja við 4 blonkum teknum. Hetta ger at tykkara svar verður innrammað í eini boks, og soleiðis brýtur frá tekstinum í uppgávuni.

    Svara tekstspurningum á hendan hátt, beint undir hvørjum slíkum spurningi.

> Tá tit eru liðug, ger hvør bólkur eitt endaligt "Run All" og síðani "Preview", sum kann opnast í browsara (og RStudio), og lata inn skjalið: "kravd1_5021_NN1_NN2.nb.html".

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### Innles pakkar

>Loada pakkar `openintro`, `tidyverse` og `janitor`. Manglar tú nakrar av hesum pakkum, so installera teir frá CRAN.

```{r, echo=TRUE}
library(openintro)
library(tidyverse)
library(janitor)
```

### Uppg. 1

> a) útskriva `email50` á einfaldasta hátt, sum tú kanst gera.

```{r include=TRUE, eval=TRUE}
email50
```

> b) somuleiðis, útskriva `email` á einfaldasta hátt, sum tú kanst gera.

```{r, include=TRUE, eval=TRUE}
email
```

> c) hvussu nógvar observatiónir og variablar eru í ávikavist `email50` og `email`?

``` 
I datasættet 'email50' er `r nrow(email50)` observationer og `r ncol(email50)` variable.
I datasættet 'email' er `r nrow(email)` observationer og `r ncol(email)` variable.
```
```{r, include=FALSE, echo=FALSE, eval=FALSE}
dt <- matrix(c(nrow(email50),ncol(email50),nrow(email),ncol(email)), nrow = 2, byrow = TRUE)
colnames(dt) = c("Antal observationer", "Antal variable")
rownames(dt) = c("email50", "email")
knitr::kable(dt, align = "cc", "html")
```

> d) vel frá `email` kolonnur: `spam`, `num_char`, `line_breaks`, `format`, og `number`, soleiðis, at bert hesar kolonnur vera útskrivaðar.

```{r}
email[,c("spam","num_char", "line_breaks", "format","number")]
select(email,"spam","num_char", "line_breaks", "format","number")
```

> e) hvørjar datatypur vísir útskriftin (d.) um variablar: `line_breaks` og `number`, og hvat slag av variablum eru hesir smb. Figure 1.7 í OpenIntro Statistics (Fourth Edition) lærubókini?

```
Udskriften i (d.) viser at variablen 'line_breaks' har datatypen '<int>', altså ikke-negative hele tal.
Ligeledes viser udskriften at variablen 'number' har datatypen '<fctr>' (forkortelse for factor), altså en variabel med kategoriske værdier.
``` 
    
> f) tel upp við brúk av `count()` og einum einføldum pipe-setningi variabul `spam` í `email`, t.v.s. hvussu nógvir emails eru spam og hvussu nógvir ikki? Royn eisini at gera líknandi uppteljing av `spam` í `email` við brúk av einum pipe-setningi, `group_by()` og `summarise()`.

```{r}
email %>%
  count(spam)
```
 
```{r message=FALSE, warning=FALSE}
email %>%
  group_by(spam) %>%
  summarize(n = n())
```

### Uppg. 2

> a) útskriva `county` data á sama hátt, sum tú gjørdi fyri `email` data.
- brúka `sum()` og `is.na()` til at telja hvussu nógv virði í variabul `smoking_ban` eru `NA`-virði?
- brúka `mean()` og `is.na()` til at rokna hvussu nógv % av øllum virðunum í variabul `smoking_ban` eru `NA`-virði?

```{r}
county

select(county,smoking_ban) %>%
  is.na() %>%
  sum()

select(county,smoking_ban) %>%
  is.na() %>%
  mean()
```

> b) 
- brúka `n_distinct()` til at telja hvussu nógv ymisk virði eru goymd í ávikavist `name` og `state`?

```{r}
select(county,name) %>%
  n_distinct(na.rm = T)

select(county,state) %>%
  n_distinct(na.rm = T)
```

    Vi har altså fundet at:
    Antallet af forskellige værdier i variablen 'name' er `r select(county,name) %>% n_distinct(na.rm = T)`.
    Antallet af forskellige værdier i variablen 'state' er `r select(county,state) %>% n_distinct(na.rm = T)`.
    

>c) 
- hvussu nógv ymisk samanhoyrandi virði eru av `name` og `state`? 

```{r}
select(county,state,name) %>%
  n_distinct(na.rm = T)
```

- tel við pipe-setningi bólkað eftir `state` talið `n` av kommunum, og sortera úrslitið eftir tali `n` frá flest til lægst.

```{r message=FALSE, warning=FALSE}
county %>%
  group_by(state) %>%
  summarize(kommuner = n()) %>%
  arrange(desc(kommuner),by_group = T)
```

- hvør statur hevur flest kommunur og hvussu nógvar?
```
    Staten Texas har flest kommuner, nemlig 256.
```
- hvør statur hevur fægst kommunur og hvussu nógvar?
```
    Staten District of Columbia har færrest kommuner, nemlig 1.
```

```{r}

```

>d) 
- tel við pipe-setningi bólkað eftir `state` talið `n` av kommunum umframt fólkatalið `state_pop10` í statunum útfrá fólkatalinum `pop2010` í kommununum. Sortera úrslitið eftir fólkatali `state_pop10` frá flest til lægst.
- hvør statur hevði hægst fólkatal grundað á tína útrokning av `state_pop10`?

```{r, message=FALSE, warning=FALSE}
#aggregate(county$pop2010, by=list(Category = county$state), FUN=sum)
county %>%
  group_by(state) %>%
  summarise(state_pop10 = sum(pop2010)) %>%
  arrange(desc(state_pop10))
```
    
    Staten Califonien har højest folketal (baseret på 'state_pop10').

>e) 
- víðka tín pipe-setning bólkað eftir `state` so tú eisini roknar miðaltalið `county_mean10` av íbúgvum pr. kommunu og brúka síðani eitt filtur, soleiðis at bert statir við minst (>=) 30 kommunum telja við. Enda pipe-setningin við at sortera frá hægst til lægst miðal.
- hvør statur við minst 30 kommunum hevur hægst miðaltal av íbúgvum pr. kommunu grundað á tína útrokning av `county_mean10`?

```{r, message=FALSE, warning=FALSE}
county %>%
  group_by(state) %>%
  summarise(kommuner = n(), state_pop10 = sum(pop2010), county_mean10 = mean(pop2010)) %>%
  filter(kommuner >= 30) %>%
  arrange(desc(county_mean10))
```

    Blandt stater med mindst 30 kommuner er Califonien den stat der har højest antal indbyggere pr. kommune (baseret på 'county_mean10').
    
> f) goym ella pipe títt úrslit omanfyri víðari til ggplot, har tú plottar `state_pop10` eftir x-aksa og `county_mean10` eftir y-aksa, sum eitt `geom_point()` scatterplot.
- eru `state_pop10` og `county_mean10` assosieraðir variablar og hví/hví ikki?

```{r message=FALSE, warning=FALSE, include=FALSE}
dt <- county %>%
  group_by(state) %>%
  summarise(kommuner = n(), state_pop10 = sum(pop2010), county_mean10 = mean(pop2010)) %>%
  filter(kommuner >= 30) %>%
  arrange(desc(county_mean10))
```
```{r}
ggplot(data = dt, aes(x = state_pop10, y = county_mean10)) + 
  geom_point() +
    geom_smooth(method="lm")
```
    Det ser ud til at 'state_pop10' og 'county_mean10' er assosierede (men det er nok ikke en lineær sammenhæng).

### Uppg. 3

> a) ger eitt `geom_point()` scatterplot av `time` mótvegis `year` í `marathon`, har tú mappar punkt-litin til `gender` (kyn), og brúkar `geom_smooth()`, so tú fært tvær bestu linjur (eina fyri hvørt kyn).
- greið frá hvørji data eru `marathon`, og hvat myndin vísir.

```{r}
glimpse(marathon)
```
    Data i 'marathon' viser løbetider for marathonløb. De variable er "year", "gener" og "time".
    
```{r}
ggplot(data = marathon, aes(x = year, y = time, color = gender)) + 
  geom_point() +
    geom_smooth(method="lm")
```
    Vi ser af ovenstående at generelt er mændenes tider lavere end kvindernes. Vi ser også at der er en tendens mod kortere tider som årene går.
    
> b) ger eitt `geom_boxplot()` av `time` mótvegis `gender` í `marathon`, so tú fært eitt boxplot fyri hvørt kyn.

```{r}
ggplot(data = marathon, aes(x = gender, y = time, color = gender)) + 
  geom_boxplot()
```

> c) ger ein pipe-setning við `marathon`, `group_by()` og `summarise()`, har tú bólkað eftir kyni roknar antal datapunkt `n`, miðal rennitíð `mean`, standard frávik `sd`, minstu rennitíð `min`, `q25 = quantile(time, 0.25)`, `median`, `q75 = quantile(time, 0.75)`, `max` og `IQR`. Goym úrslitið av tíni pipe í `marathon_summary` og skriva út úrslitið.

<!-- XXX 8. sept. mangler stort set resten herfra... -->

```{r}
marathon %>%
  group_by(gender) %>%
  summarise(datapunkter = n(), renni_mean = mean(time), std_afvi = sd(time))
```

>d) ger eina pipe við `marathon_summary` har tú við einum `mutate` leggur eitt ovara mark `w_max` svarandi til markið har `geom_boxplot()`vil plotta einstøk óvanlig punkt sum outliers.

```{r}

```

>e) ger eitt filtur á `marathon` og útskriva høgar outliers (> `w_max`) fyri ávikavist kvinnur og menn.

```{r}

```

> f) hvat kann sigast um hesar outliers fyri kvinnur og menn í `marathon`, t.d. heldur tú hetta eru feilir ella álítandi data?

### Uppg. 4

> a) ger eitt einfalt (default) `geom_dotplot()` av `num_char` í `email50` data.

```{r}
ggplot(data = email50, aes(x = num_char)) + 
  geom_dotplot(binaxis='x')
```

> b) ger eitt einfalt (default) `geom_histogram()` av `num_char` í `email50` data.

```{r}
ggplot(data = email50, aes(x = num_char)) + 
  geom_histogram()
```

> c) ger eitt `geom_histogram()` av `num_char` í `email50` data, har tú setur eina inndeiling (`breaks`) sum ein sekvens frá 0 til 65 í stigum á 5.

```{r}
ggplot(data = email50, aes(x = num_char)) + 
  geom_histogram(breaks = seq(0, 65, by = 5))
```

> d) ger eina pipe av `email50` til `count()`, har tú við hjálp av `cut_width()` telur talið í hvørji inndeiling av `num_char` (brúka `width = 5` og `boundary = 0`). Eftirkanna at tín uppteljing samsvarar við títt histogram.

```{r}

```

> e) ger eitt einfalt `summary()` av variabul `num_char` (`email50$num_char`).
- hvat eru Median- og Mean-virðini ?
- hví eru tey ikki eins (ella næstan eins) og hvat kann tað vera tekin um?
- rokna `IQR()` av `email50$num_char` og greið frá hvat hetta virði merkir.
- rokna `sd()` av `email50$num_char` og greið frá hvat hetta virði merkir.

```{r}
summary(email50$num_char)
```
    Medianen er 6.889 og middelværdien er 11.598. Forskellen på median og middelværdi kan være tegn på ekstreme værdier, da ekstreme værdier påvirker middelværdien mere end de påvirker medianen.  
    
    Værdien af Interquartile Range (IQR) er: `r IQR(email50$num_char)`. Denne værdi viser forskellen på øvre- og nedre kvartil. Det betyder at 50 procent af observationerne ligger inden for et interval på `r IQR(email50$num_char)`.  
    
    Standard afvigelsen er `r sd(email50$num_char)`.
    
> f) ger eitt `geom_boxplot()` av variabul `y = num_char` frá `email50` data, uttan at seta nakra mapping fyri `x`.

```{r}
ggplot(data = email50, aes(y = num_char)) + 
  geom_boxplot()
```


### Uppg. 5

> a) ger aftur eitt `geom_boxplot()` av variabul `num_char` frá `email50`, men har tú í aes-mappingini setur `x = ""` og annars gevur x-aksanum heitið "Boxplot".

```{r}

```

> b) ger nú sama boxplot av variabul `num_char` frá `email50`, men har tú omanyvir boxplottið eisini plottar einstøku punktini. Brúka `shape = 1` og `size = 2` uttan at seta nakran lit fyri punktini.

```{r}

```

> c) ger nú eitt boxplot av variabul `num_char` frá `email50`, har tú bert brúkar tey 24 `num_char`-virðini, sum eru størri enn 3 og minni enn 20. Omanyvir boxplottið plottar tú eisini tey 24 punktini av `num_char`, og snara boxplottinum við `coord-flip()`. Útfrá tínum boxplotti, met um:
- hvussu nógv punkt eru ávikavist innanfyri og uttanfyri boksina?
- hvussu nógv punkt eru hvørjumegin medianin?
- hvussu er punktmongdin annars deild upp í hesum boxplotti?

```{r}

```

> d) ger við brúk av pipe eina contingency-talvu við uppteljing av `spam, number` úr `email` data (ikki `email50`). Brúka `count()` og `spread()` í tíni pipe, soleiðis, at tú fært útlisið eina talvu við 4 kolonnum og tveimum rekkjum av úrslitum. Legg síðani við brúk av `adorn_totals()` bæði rekkju- og kolonnu-totalar afturat tíni talvu, og útskriva talvuna við `knitr::kable()`.

```{r}

```

> e) arbeið víðari uppá tína pipe av `email`, soleiðis, at tú við brúk av `adorn_percentages()` umroknar tølini til brøkpartar í mun til kolonnu-totalar, og útskrivar talvuna við `knitr::kable()`.
- hvussu nógv % av øllum teldupostum í `email` eru spam?
- hvussu nógv % av teldupostum í `email` har `number` er ávikavist `none`, `small`, `big` eru spam?
- eru variablarnir `spam` og `number` heftir ella óheftir og hví?

```{r}

```

> f) 
- ger eitt `geom_bar()` plot av `number`, har `fill = factor(spam)`.
- ger eitt tilsvarandi `geom_bar()` plot, har tú eisini setur `position = "fill"`.

```{r}

```
