library(tidyverse)
library(openintro)
library(cowplot)
## Ex. 1
# 1a
# Create body-tibble
body <- bdims %>% select(age, sex, wgt, hgt, wai_gi)
# 1b
# Change sex from integer til factor
body$sex <- factor(body$sex, levels=c(0,1))
# Recode factoring
body$sex <- fct_recode(body$sex, f="0", m="1")

# 1c
# use ggplot() and the data in body to make a simple scatterplot of the weight versus the waist (x = wai_gi andy = wgt).
ggplot(data = body, aes(x=wai_gi, y=wgt)) +
  geom_point()

# 1d
# again, make a simple scatterplot of the weight versus the waist (x = wai_gi andy = wgt) in which you let ggplot() automatically colour the data points by variable sex.
ggplot(data = body, aes(x=wai_gi, y=wgt, color=sex)) +
  geom_point()

# 1e
# make a scatterplot of the weight versus the waist (x = wai_gi andy = wgt) in which you let ggplot() automatically divide the plot in two plots (facets) by variable sex.
ggplot(data = body, aes(x=wai_gi, y=wgt, color = sex)) +
  geom_point() +
  facet_wrap(~ sex, nrow = 2)

# 1f
# make a histogram of variable hgt using binwidth = 5 in two vertically stacked plots (facets) by variable sex.
ggplot(data = body, aes(x=hgt, color = sex)) +
  geom_histogram(binwidth = 5) +
  facet_wrap(~ sex, nrow = 2)
# in one plot create a frequency plot of hgt using binwidth = 5 with two curves coloured by variable sex.
ggplot(data = body, aes(x=hgt, color = sex)) +
  geom_freqpoly(binwidth = 5)


## Ex. 2
# 2a
# make a pipe of body to summarise the number n of observations and to compute for variable age: the mean, sd, min, q1, median, q3, max and IQR. Your result should be 9 columns of variables (n, mean, sd, min, q1, median, q3, max and IQR) with one row of summarised observations (tip: use the quantile() function to calculate the quantiles). Save the results of the pipe in tibble all_age and output all_age.
all_age <- body %>% summarise(
  count = n(),
  mean = mean(age),
  sd = sd(age),
  #min = unname(quantile(age))[1],
  min = min(age),
  q1 = quantile(age,0.25),
  median = median(age),
  q3 = quantile(age,0.75),
  max = max(age),
  IQR = IQR(age)
)
all_age

# 2b
# now make a pipe of body that is grouped_by variable sex to summarise the number n of observations and to compute for variable age: the mean, sd, min, q1, median, q3, max and IQR. Your result should be 10 columns of variables (sex, n, mean, sd, min, q1, median, q3, max and IQR) with 2 rows of summarised observations - one row for each factor level of variable sex. Save the results of the pipe in tibble sex_age and output sex_age.
sex_age <- body %>%  group_by(sex) %>%
  summarise(
    number = n(),
    mean = mean(age),
    sd = sd(age),
    min = min(age),
    q1 = quantile(age,0.25),
    median = median(age),
    q3 = quantile(age,0.75),
    max = max(age),
    IQR = IQR(age)
  )
sex_age

# 2c
#  make a boxplot of variable age in body versus variable sex.
ggplot(body, aes(x=sex, y=age, color=sex)) +
  geom_boxplot() +
  geom_jitter(aes(color = sex, shape = sex))

# 2d
# re-use the pipe from a) by replacing variable age with variable hgt. This time, save the results of the pipe in the tibble all_hgt and output all_hgt that in one row summarises the heights for all records in body.
all_hgt <- body %>% summarise(
  count = n(),
  mean = mean(hgt),
  sd = sd(hgt),
  min = min(hgt),
  q1 = quantile(hgt,0.25),
  median = median(hgt),
  q3 = quantile(hgt,0.75),
  max = max(hgt),
  IQR = IQR(hgt)
)
all_hgt
# similarly, re-use the pipe from b) by replacing variable age with variable hgt. This time, save the results of the pipe in the tibble sex_hgt and output sex_hgt that in two rows summarises the heights for all records in body - one row for each level of variable sex.
sex_hgt <- body %>%  group_by(sex) %>%
  summarise(
    count = n(),
    mean = mean(hgt),
    sd = sd(hgt),
    min = min(hgt),
    q1 = quantile(hgt,0.25),
    median = median(hgt),
    q3 = quantile(hgt,0.75),
    max = max(hgt),
    IQR = IQR(hgt)
  )
sex_hgt

# 2e
# make a boxplot of variable hgt in body versus variable sex.
ggplot(body, aes(x=sex, y=hgt, color=sex)) +
  geom_boxplot() +
  geom_jitter()

# 2f
# from the boxplots in c) and e), are theage or the hgt distributions best approximated by the normal distribution and why? (tip: you may also use the tibbles sex_age and sex_hgt from b) and d) to help you answer this question).
# her bruges library(cowplot) til at vise to plots ved siden af hinanden
plot1 <- ggplot(body, aes(x=sex, y=age, color=sex)) +
  geom_boxplot() +
  geom_jitter()

plot2 <- ggplot(body, aes(x=sex, y=hgt, color=sex)) +
  geom_boxplot() +
  geom_jitter()
plot_grid(plot1, plot2, labels= "AUTO")
sex_age
sex_hgt
# SVAR: 
# For age ser vi på boxplottene at observationerne er moderat right-screwed (der er lang hale mod høje værdier).
# Vi kan også se på boksplottne se at IQR ikke er fordelt symmetrisk om medianen.
# Dette bekræftes af at der er (betydelig) forskel på "mean" og "median". Ekstreme værdier påvirker "mean" mere end de påvirker "median".
# Dette kan indikere at data afviger fra en normalfordeling.
# For hgt er "mean" og "median" tættere på hinanden, og IQR er fordelt (nært) symmertisk omkring medianen.
# Altså er hgt bedst tilnærmet med en normalfordeling.

# make a qq-plot for the variable age in body coloured by sex using ggplot()and stat_qq().
ggplot(body, aes(sample = age, color = sex)) +
  stat_qq() +
  labs(y = "Age")
# make a qq-plot for the hgt data in body coloured by sex using stat_qq. In this case, also add lines coloured by sex using stat_qq_line().
ggplot(body, aes(sample = hgt,color = sex)) +
  stat_qq() +
  labs(y = "Height") +
  stat_qq_line()
# do the qq-plots confirm your answer about: if the age or the hgtdistributions for each sex are best approximated by the normal distribution?
# SVAR:
# Ja.

## Ex. 3
# 3a
#  only for the males in body, plot one histogram of hgt using y = ..density.. and a binwidth of 5. (Note: we do not plot two histograms here).
body_male <- filter(body,sex == "m") 
body_female <- filter(body,sex == "f") 
ggplot(body_male, aes(x = hgt, y = ..density..)) +
  geom_histogram(binwidth = 5)
# 3b
# extract the mean and sd of male hgt from tibble sex_hgt (tip: use filter(sex_hgt, insert your code here)$variable_name, or use sex_hgt$variable_name[insert your index here]).
sex_hgt$mean[2]
sex_hgt$sd[2]
ggplot(body_male, mapping = aes(x = hgt)) +
  geom_histogram(aes(y = ..density..), binwidth = 5) +
  stat_function(fun = dnorm, args = list(mean = sex_hgt$mean[2], sd = sex_hgt$sd[2]))
# 3c
# consider the two conditions (Independency, Normality)-conditions required to apply the Central Limit Theorem for The Sample Mean.
# are these conditions fulfilled for the sampling distribution of the mean hgt in body of females and males, respectively?

   # Independency: Dette er (nok) opfyldt da observationerne er fra tilfældig udvælgelse af både kvinder og mænd.
   # Success-failure condition: Er opfyldt da np > 10 og n (1-p) > 10, for både kvinder og mænd.

# 3d
# based on t-confidence intervals for the mean, compute the 95%-confidence interval for the mean hgt of females and males in the sample body by using a pipe of sex_hgt to a select() of variables sex, n, mean, sd and then a mutate() to add the computed variables t_df, SE, c_low and c_high (Tip: t_df = qt(0.975, n-1)).
z_95 <- 1.96
n_sample <- all_hgt$count
sex_hgt %>% 
  select(sex, count, mean, sd) %>%
  mutate(
    t_df = qt(0.975, n_sample - 1),
    SE = sqrt( (count / n_sample) * (1 - (count / n_sample)) / n_sample), 
    c_low = (count / n_sample) - z_95*SE, 
    c_high = (count / n_sample) + z_95*SE)
# 3e
# similarly, based on t-confidence intervals for the mean, compute the 95%-confidence interval for the mean hgt of all in sample body by using a pipe of all_hgt to a select() of variables n, mean, sd and then a mutate() to add the computed variables t_df, SE,  c_low and c_high.
all_hgt %>% 
  select(count, mean, sd) %>%
  mutate(
    t_df = qt(0.975, n_sample - 1),
    SE = sqrt( (count / n_sample) * (1 - (count / n_sample ) ) / n_sample), 
    c_low = (count / n_sample) - z_95*SE, 
    c_high = (count / n_sample) + z_95*SE)
all_hgt

# 3f
# investigate the distribution of the hgt variable of all in body without any division by sex:
#   - make a single histogram of hgt with an overlaid normal distribution, similarly to the plot in b)
ggplot(body, aes(x = hgt)) +
  geom_histogram(aes(y = ..density..), binwidth = 5) +
  stat_function(fun = dnorm, args = list(mean = mean(body$hgt), sd = sd(body$hgt)))
#   - make a single qq-plot of hgt.
ggplot(body, aes(sample = hgt)) +
  stat_qq() +
  stat_qq_line()
#   - do you think the distribution of hgt is: normal (symmetric), nearly-normal (slight/moderate skew) or far from the normal distribution (strong skew)?
# SVAR:
# Ja, hgt ser ud til at være normalfordelt, dog med en moderat skew mod højre (lang hale mod høje værdier)
  
### XXX MANGLER AT FYLDE IND HERFRA OG RESTEN 
## Ex. 4
# 4a
pop_sample <- tibble(sex = sample(pop_fo$sex, 500))
pop_sample

### Leg med Ex. 5
# 5c
# similarly, in one line of code, make a pipe of body to summarise_at() in which you simultanously calculate mean and sd for the variables age and hgt in body, but now grouped_by variable sex and pipe your sentence into select() to name and reorder the columns in the following order: sex, age_mean, age_sd, hgt_mean, hgt_sd.
body %>%
  group_by(sex) %>%
  summarise_at(vars(age, hgt), list(mean = "mean", sd = "sd")) %>%
  select(sex, age_mean, age_sd, hgt_mean, hgt_sd)

# 5d
# compute the estimate and 95%-confidence interval for hgt in body using a one-sample t.test().
t.test(body$hgt)

# compare the confidence interval from the test with the confidence interval in Exercise 3 e): are they similar?
# SVAR:
# Ja, det er ens.

# 5e
# compute the estimate and 95%-confidence interval for age in body using a one-sample t.test().
t.test(body$age)

# 5f
# compute the estimate and 95%-confidence interval for the difference in hgt in body between males and females using a two-sample t.test().
t.test(body_male$hgt, body_female$hgt)
# are the conditions for using this test fulfilled? What is the null hypothesis of the test? Is it rejected?
# SVAR:
# Ja, betingelserne er opfyldt for height.
# For the results of a two sample t-test to be valid, the following assumptions should be met:
# - The observations in one sample should be independent of the observations in the other sample.
# - The data should be approximately normally distributed.
# - The two samples should have approximately the same variance.
#
# Nul-hypotesen for testen er at de to populationer (dvs. mænd og kvinder) har samme middelværdi ("mean").
# Ja, vi forkaster nul-hypotesen på et 5 procent signifikans-niveau (p-værdi = 2.2E-16)


